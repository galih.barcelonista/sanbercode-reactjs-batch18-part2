import React, {Component} from "react"

class List extends Component{

    constructor(props){
      super(props)
      this.state ={
       pesertaLomba : [ 'Budi', 'Susi', 'Lala', 'Agung' ],
       inputPeserta:"",
       index: -1
      }

      //bind in constuctor
      this.handleEdit = this.handleEdit.bind(this)
    }

    //untuk menambah
    handleSubmit(event){
    event.preventDefault()

    let index = this.state.index
    let inputPeserta = this.state.inputPeserta
    let pesertaLomba= this.state.pesertaLomba

    if(index === -1){
        this.setState({
            pesertaLomba: [...pesertaLomba,inputPeserta],inputPeserta:""
        })
    }else{
       pesertaLomba[index]= inputPeserta
        this.setState({
            pesertaLomba: pesertaLomba,inputPeserta:""
        })
    }
    }

    handleChange = (event) =>{
        var value = event.target.value
        this.setState({
            inputPeserta: value
        })
    }

    //untuk edit
    handleEdit(event){
        let index = event.target.value
        this.setState({
            inputPeserta: this.state.pesertaLomba[index],index
        })
    }

    //untuk delet
    handleDelete = (event) =>{
        let index = event.target.value;
        this.state.pesertaLomba.splice(index,1)

        this.setState({pesertaLomba: this.state.pesertaLomba})

    }
  
    render(){
      return(
        <div style={{margin:"0 auto",width:"80%"}}>
          <h1>Daftar Peserta Lomba</h1>
          <table>
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                {
                  this.state.pesertaLomba.map((val, index)=>{
                    return(                    
                      <tr>
                        <td>{index+1}</td>
                        <td>{val}</td>
                        <td>
                            <button value={index} onClick={this.handleEdit}>Edit</button>
                            <button style={{marginLeft:"1em"}} value={index} onClick={this.handleDelete}>Delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
            </tbody>
          </table>
          <br />
          <form onSubmit={this.handleSubmit.bind(this)}>
            <label>
                Masukkan nama peserta:
            </label>          
            <input type="text" required onChange={this.handleChange} value={this.state.inputPeserta} />
            <input type="submit" value="Submit" />
            </form>

        </div>
      )
    }
  }

  export default List 