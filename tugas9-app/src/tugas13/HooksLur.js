import React, {useEffect, useState} from "react"

const HooksLur = () =>{
    const [text, setText]=useState("Header")

    useEffect(() => {
        document.title = `${text}`;
    });

    const onInputChange = (ev) =>{
        let value = ev.target.value;
        setText(value)
    }
    return(
        <div style={{textAlign:"center"}}>
            <h1>{text}</h1>
            <br />
            <input type="text" onChange={onInputChange} />
        </div>
    )
}

export default HooksLur