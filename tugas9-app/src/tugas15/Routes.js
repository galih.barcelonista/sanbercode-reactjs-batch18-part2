import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
 import Tugas9 from '../tugas9/tugas9';
import Tugas10 from '../tugas10/Tugas10'
import Tugas11 from '../tugas11/Timer';
import Tugas12 from '../tugas12/ListBuah';
import Tugas13 from '../tugas13/HooksListBuah';
import Tugas14 from '../tugas14/Buah';


const Routes = () => {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li><Link to="/Tugas10">Tugas 10</Link></li>
            <li><Link to="/Tugas11">Tugas 11</Link></li>
            <li><Link to="/Tugas12">Tugas 12</Link></li>
            <li><Link to="/Tugas13">Tugas13</Link></li>
            <li><Link to="/Tugas14">Tugas 14</Link></li>

          </ul>
        </nav>

        <Switch>
{/* 
          contoh route dgn component sbg child */}
          <Route exact path="/">
            <Tugas9 />
          </Route>
          <Route exact path="/tugas10" component={Tugas10} />
          <Route exact path="/tugas11" component={Tugas11} />
          <Route exact path="/tugas12" component={Tugas12} />
          <Route exact path="/tugas13" component={Tugas13} />
          <Route exact path="/tugas14" component={Tugas14} />
        </Switch>
      </div>
    </Router>
  );

}

export default Routes