import React from "react"
import {BuahProvider} from "./BuahContext"
import BuahList from "./BuahList"
import BuahForms from "./BuahForms"


const Buah = ()=>{

    return(
        <div>
        <BuahProvider>
        <BuahList/>
          <BuahForms/>
        </BuahProvider>
        </div>
    )
}


export default Buah