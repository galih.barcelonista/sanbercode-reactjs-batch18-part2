import React, {useContext, useState} from "react"
import {BooksContext} from './BooksContext'

const BooksForm = ()=>{
  const [books , setBooks] = useContext(BooksContext)
  const [input, setInput] = useState({name: "", totalPage: "", bookType: ""})

  const onInputChange = (event) =>{
    let type = event.target.name
    let value = event.target.value

    switch(type){
      case "name": {setInput({...input, name: value});break;}
      case "totalPage": {setInput({...input, totalPage: value});break;}
      case "bookType": {setInput({...input, bookType: value});break;}
    }
  }

  
  const onSubmitForm = (event) =>{
    event.preventDefault()

    setBooks([...books, {name: input.name, totalOfPage: input.totalPage, typeOfBook: input.bookType}])
    setInput({name: "", totalPage: "", bookType: ""})
  }

  return(
    <>
      <h1 style={{marginTop: "20px", textAlign: "center"}}>Add Book</h1>
      <form onSubmit={onSubmitForm} style={{textAlign: "center"}}>        
        <strong style={{marginRight: "20px"}}>Name</strong><input required name="name" onChange={onInputChange}  /><br/>
        <strong style={{marginRight: "20px"}}>Total Page</strong><input required type="number" name="totalPage" onChange={onInputChange} /><br/>
        <strong style={{marginRight: "20px"}}>Book Type</strong><input required name="bookType" onChange={onInputChange} /><br/>
        <br/>
        <button>Submit</button>
      </form>
    </>
  )
}

export default BooksForm
