import React, { useState, createContext } from "react";

export const BooksContext = createContext();

export const BooksProvider = props => {
  const [books,setBooks] = useState([
    { name: "Harry Potter", totalOfPage: 120,typeOfBook:"text book"},
    { name: "Sherlock Holmes", totalOfPage: 125,typeOfBook:"text book"},
    { name: "Avengers", totalOfPage: 130,typeOfBook:"novel"},
    { name: "Spiderman", totalOfPage: 124,typeOfBook:"film"},
  ]);

  return (
    <BooksContext.Provider value={[books,setBooks]}>
      {props.children}
    </BooksContext.Provider>
  );
};