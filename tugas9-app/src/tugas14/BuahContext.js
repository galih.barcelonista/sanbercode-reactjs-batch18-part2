import React, { useState, createContext } from "react";

export const BuahContext = createContext();

export const BuahProvider = props =>{
const [buah,setBuah] = useState({
    lists: null,
    selectedId: 0,
    statusForm: "create"
})

return(
    <BuahContext.Provider value={[buah,setBuah]}>
        {props.children}
    </BuahContext.Provider>
)
}