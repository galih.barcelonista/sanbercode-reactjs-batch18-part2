import React, {useContext, useState, useEffect} from "react"
import {BuahContext} from './BuahContext'
import axios from "axios"

const BuahForms = () =>{
    const [buah,setBuah] = useContext(BuahContext)
    const [input, setInput] = useState({name:"",price:"",weight:0,id:null})

    useEffect(()=>{
        if (buah.statusForm === "changeToEdit"){
          let dataBuah = buah.lists.find(x=> x.id === buah.selectedId)
          setInput({name: dataBuah.name, price: dataBuah.price, weight: dataBuah.weight})
          setBuah({...buah, statusForm: "edit"})
        }
      },[buah, setBuah])
    

      const handleChange = (event) =>{
        let typeOfInput = event.target.name
    
        switch (typeOfInput){
          case "name":
          {
            setInput({...input, name: event.target.value});
            break
          }
          case "price":
          {
            setInput({...input, price: event.target.value});
            break
          }
          case "weight":
          {
            setInput({...input, weight: event.target.value});
              break
          }
        default:
          {break;}
        }
      }
      
      const handleSubmit = (event) =>{
        // menahan submit
        event.preventDefault()
    
        let name = input.name
        let price = input.price.toString()
        
    
        if (buah.statusForm === "create"){        
          axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name, price, weight: input.weight})
          .then(res => {
              setBuah(
                {statusForm: "create", selectedId: 0,
                lists: [
                  ...buah.lists, 
                  { id: res.data.id, 
                    name: input.name, 
                    price: input.price,
                    weight: input.weight
                  }]
                })
          })
        }else if(buah.statusForm === "edit"){
          axios.put(`http://backendexample.sanbercloud.com/api/fruits/${buah.selectedId}`, {name, price, weight: input.weight})
          .then(() => {
              let dataBuah = buah.lists.find(el=> el.id === buah.selectedId)
              dataBuah.name = input.name
              dataBuah.price = input.price
              dataBuah.weight = input.weight
              setBuah({statusForm: "create", selectedId: 0, lists: [...buah.lists]})
          })
        }
    
        setInput({name: "", price: "", weight: 0})
    
      }
    
  
  return(
      <>
       <h1>Form Daftar Harga Buah</h1>
          <div style={{width: "50%", margin: "0 auto", display: "block"}}>
            <div style={{border: "1px solid #aaa", padding: "20px"}}>
              <form onSubmit={handleSubmit}>
                <label style={{float: "left"}}>
                  Nama:
                </label>
                <input style={{float: "right"}} type="text" required name="name" value={input.name} onChange={handleChange}/>
                <br/>
                <br/>
                <label style={{float: "left"}}>
                  Harga:
                </label>
                <input style={{float: "right"}} type="text" required name="price" value={input.price} onChange={handleChange}/>
                <br/>
                <br/>
                <label style={{float: "left"}}>
                  Berat (dalam gram):
                </label>
                <input style={{float: "right"}} type="number" required name="weight" value={input.weight} onChange={handleChange}/>
                <br/>
                <br/>
                <div style={{width: "100%", paddingBottom: "20px"}}>
                  <button style={{ float: "right"}}>submit</button>
                </div>
              </form>
            </div>
          </div>


      </>
  )
}


export default BuahForms