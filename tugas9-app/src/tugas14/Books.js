import React from "react"
import {BooksProvider} from "./BooksContext"
import BooksList from "./BooksList"
import BooksForm from "./BooksForm"


const Books = ()=>{

    return(
        <div>
        <BooksProvider>
          <BooksList/>
          <BooksForm/>
        </BooksProvider>
        </div>
    )
}


export default Books