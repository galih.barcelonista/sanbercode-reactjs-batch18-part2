import React, { useContext,useEffect } from "react"
import {BuahContext} from './BuahContext'
import {BuahForms} from './BuahForms'
import axios from "axios"

const BuahList = () =>{
    const [buah,setBuah] = useContext(BuahContext)
    useEffect( () => {
        if (buah.lists === null){
          axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
          .then(res => {
            setBuah({
              ...buah, 
              lists: res.data.map(el=>{ 
                return {id: el.id,
                  name: el.name, 
                  price: el.price, 
                  weight: el.weight 
                }
              })
            })
          })
        }
      }, [setBuah, buah])
    
      const handleEdit = (event) =>{
        let idDataBuah = parseInt(event.target.value)
        setBuah({...buah, selectedId: idDataBuah, statusForm: "changeToEdit"})
      }
    
      const handleDelete = (event) => {
        let idDataBuah = parseInt(event.target.value)
    
        let newLists = buah.lists.filter(el => el.id !== idDataBuah)
    
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idDataBuah}`)
        .then(res => {
          console.log(res)
        })
              
        setBuah({...buah, lists: [...newLists]})
        
      }
    

    return(
        <>
             <h1>Daftar Harga Buah</h1>
          <table>
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                {
                    buah.lists !== null && (
                  buah.lists.map((item,index) => {
                    return(                    
                      <tr key={index}>
                        <td>{index+1}</td>
                        <td>{item.name}</td>
                        <td>{item.price}</td>
                        <td>{item.weight/1000} kg</td>
                        <td>
                          <button onClick={handleEdit} value={item.id}>Edit</button>
                          &nbsp;
                          <button onClick={handleDelete} value={item.id}>Delete</button>
                        </td>
                      </tr>
                    )
                  })
                    )}
            </tbody>
          </table>

        </>
    )

}



export default BuahList