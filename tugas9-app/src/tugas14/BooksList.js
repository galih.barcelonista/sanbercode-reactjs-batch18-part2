import React, { useContext } from "react"
import {BooksContext} from './BooksContext'

const BooksList = ()=>{
    const [books] = useContext(BooksContext)

    return(
        <>
            <h1>List of Books</h1>
            {books.map(el=>{
                return (
                    <li>
                        {`${el.name} with ${el.totalOfPage} page, ${el.typeOfBooks}`}
                    </li>
                )
            })}
        </>
    )
}

export default BooksList